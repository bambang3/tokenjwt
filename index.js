const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();

app.get('/api', function (req, res) {
    res.json({
        text: 'my api'
    });
});

app.post('/api/login', function (req, res) {
    const user = { 
                    id: 3 ,
                    nama: 'bam', 
                    email: 'bam@gmail.com'
                };
    const token = jwt.sign({ user }, 'kunciRahasia');
    res.json({ token });
});
app.get('/api/protected', ensuretoken, function (req, res) {
    jwt.verify(req.token, 'kunciRahasia', function (err, data) {
        if (err) {
            res.sendStatus(403);
        } else {
            res.json({
                text: 'this protected', 
                data
            })
        }
    });
});

function ensuretoken(req, res, next) {
    const brearerHeader = req.headers["authorization"];
    if (typeof brearerHeader !== 'undefined') {
        const bearer = brearerHeader.split(" ");
        const bearertoken = bearer[1];
        req.token = bearertoken;
        next();
    } else {
        res.sendStatus(403);
    }
}

app.listen(3000, function () {
    console.log('listening on port 3000');
})